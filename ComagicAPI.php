<?php

namespace Comagic;

class ComagicAPI
{
    private $URL = 'https://dataapi.comagic.ru/v2.0'; // Адрес для отправки запросов

    private $token; // токен авторизации
    private $lastResponse; // последний ответ от API

    /**
     * Создание элемента класса
     *
     * @param string $token Токен авторизации
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Запрос к API
     *
     * @param array $data Массив параметров запроса
     * @return array
     */
    private function callCurl($data)
    {
        $params = json_encode($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        $res = json_decode($res, 1);

        $this->lastResponse = $res;
        
        if (isset($res['result'])) {
            return $res['result'];
        }

        return $res;
    }

    /**
     * Вызов метода API
     *
     * @param string $method Название метода
     * @param array $params Параметры запроса
     * @return array
     */
    public function request($method, $params = [])
    {
        $data["jsonrpc"] = "2.0";
        $data["id"] = "number";
        $data["method"] = $method;

        $data["params"] = $params;

        $data["params"]["access_token"] = $this->token;

        $res = $this->callCurl($data);

        return $res;
    }

    /**
     * Получить последний ответ от сервера
     *
     * @return array
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Получение информации о сессии посетителя
     *
     * @param string $userID ID посетителя
     * @param string $dateFrom Дата начала выборки
     * @param string $dateTo Дата окончания выборки
     * @param string $select Массив необходимых полей
     * @return array
     */
    public function getUserSessions($userID, $dateFrom, $dateTo, $select = ["date_time", "visitor_city"])
    {
        $params = [
            "filter" => [
                "field" => "visitor_id",
                "operator" => "=",
                "value" => $userID
            ],
            "fields" => $select,
            "date_from" => $dateFrom,
            "date_till" => $dateTo
        ];

        $res = $this->request('get.visitor_sessions_report', $params);

        return $res;
    }

    /**
     * Получить обращения пользователя
     *
     * @param string $userID ID посетителя
     * @param string $dateFrom Дата начала выборки
     * @param string $dateTo Дата окончания выборки
     * @param array $select Массив необходимых полей
     * @return array
     */
    public function getUserCommunication($userID, $dateFrom, $dateTo, $select = ["communication_type", "date_time"])
    {
        $params = [
            "filter" => [
                "field" => "visitor_id",
                "operator" => "=",
                "value" => $userID
            ],
            "fields" => $select,
            "date_from" => $dateFrom,
            "date_till" => $dateTo
        ];

        $res = $this->request("get.communications_report", $params);

        return $res;
    }
}
